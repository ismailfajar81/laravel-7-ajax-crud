variables:
  DOCKER_REGISTRY: 607034314775.dkr.ecr.ap-southeast-1.amazonaws.com
  AWS_DEFAULT_REGION: ap-southeast-1
  APP_NAME: ajax
  DOCKER_HOST: tcp://docker:2375
  APP_CONTAINER_NAME: ajax-app
stages:
  - preparation
  - build_image
  - build_image_with_tags
  - deploy_staging
  - deploy_heroku
  - deploy_production
  - migrate_db_staging
  - migrate_db_prod
  - migrate
  - cleanup_table

preparation:
  image: lorisleiva/laravel-docker:7.4
  stage: preparation
  only:
    - master
  script:
    - composer install --prefer-dist --no-ansi --no-interaction --no-progress --no-scripts
    - composer dump-autoload --optimize --classmap-authoritative
    - cp .env.example .env
    - php artisan key:generate
    - phpunit --colors=never

build_image:
  image:
    name: amazon/aws-cli
    entrypoint: [ "" ]
  stage: build_image
  services:
    - docker:19.03.12-dind
  before_script:
    - amazon-linux-extras install docker
    - aws --version
    - docker --version
  script:
    - aws ecr get-login-password | docker login --username AWS --password-stdin $DOCKER_REGISTRY
    - docker build -f docker/app/Dockerfile -t $DOCKER_REGISTRY/$APP_NAME:$CI_PIPELINE_IID .
    - docker push $DOCKER_REGISTRY/$APP_NAME:$CI_PIPELINE_IID
  only:
    - master

build_image_with_tags:
  only:
    - tags
  image: docker:19.03.8
  services:
    - docker:19.03.12-dind
  stage: build_image_with_tags
  before_script:
    - apk add --no-cache curl jq python py-pip
    - pip install awscli
  script:
    - $(aws ecr get-login --no-include-email --region ap-southeast-1)
    - docker build -f docker/app/Dockerfile -t $REPOSITORY_ECR_URL:$CI_PIPELINE_IID .
    - docker tag $REPOSITORY_ECR_URL:$CI_PIPELINE_IID $REPOSITORY_ECR_URL:$CI_COMMIT_REF_NAME
    - docker push $REPOSITORY_ECR_URL

Deploy to Heroku:
  image: lorisleiva/laravel-docker:7.4
  stage: deploy_heroku
  script:
    - apk add ruby ruby-master ruby-irb ruby-rake ruby-io-console ruby-bigdecimal ruby-json ruby-bundler yarn ruby-rdoc >> /master/null
    - apk update
    - gem install dpl >> /master/null
    - dpl --provider=heroku --app=$HEROKU_APP --api-key=$HEROKU_API_KEY
  only:
    - master
  when: manual

Deploy Staging:
  image: ubuntu:latest
  stage: deploy_staging
  before_script:
    - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client rsync -y )'
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
    - rsync -avuz $CI_PROJECT_DIR/docker/docker-compose.yml $USER@$SERVER_DEV:~/docker-compose.yml
    - rsync -avuz $CI_PROJECT_DIR/.env.example $USER@$SERVER_DEV:~/.env
  script:
    - pwd
    - ssh $USER@$SERVER_DEV "aws ecr get-login-password --region ap-southeast-1 | docker login --username AWS --password-stdin $DOCKER_REGISTRY; export DEV_DB_CONNECTION=$DEV_DB_CONNECTION; export DEV_DB_HOST=$DEV_DB_HOST; export DEV_DB_PORT=$DEV_DB_PORT; export DEV_DB_DATABASE=$DEV_DB_DATABASE; export DEV_DB_USERNAME=$DEV_DB_USERNAME; export DEV_DB_PASSWORD=$DEV_DB_PASSWORD; export DEV_PMA_HOST=$DEV_PMA_HOST; export TAG_APP=$CI_PIPELINE_IID; docker-compose down; docker system prune -a -f; docker pull $REPOSITORY_ECR_URL:$CI_PIPELINE_IID; docker-compose up -d; docker logout"
  only:
    - master

Migrate Database Staging:
  image: ubuntu:latest
  stage: migrate_db_staging
  before_script:
    - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
  script:
    - pwd
    - ssh $USER@$SERVER_DEV "docker exec $APP_CONTAINER_NAME php artisan key:generate; docker exec $APP_CONTAINER_NAME php artisan migrate --force"
  only:
    - master

Deploy Production:
  image: ubuntu:latest
  stage: deploy_production
  before_script:
    - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
  script:
    - pwd
    - ssh $USER@$SERVER_PROD "aws ecr get-login-password --region ap-southeast-1 | docker login --username AWS --password-stdin $ECR_URL; export TAG_DIBAYAR=$CI_COMMIT_REF_NAME; docker-compose down; docker system prune -a -f; docker pull $REPOSITORY_ECR_URL:$CI_COMMIT_REF_NAME; docker-compose up -d; docker logout"
  only:
    - /-RELEASE$/

Migrate Database Production:
  image: ubuntu:latest
  stage: migrate_db_prod
  before_script:
    - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
  script:
    - pwd
    - ssh $USER@$SERVER_PROD "docker exec $APP_CONTAINER_NAME php artisan migrate --force; docker exec $APP_CONTAINER_NAME php artisan optimize"
  only:
    - /-RELEASE$/

Migrate Table:
  image: lorisleiva/laravel-docker:latest
  stage: migrate
  script:
    - yarn global add heroku@7.42.1
    - heroku run php artisan migrate --exit-code --app $HEROKU_APP
    - heroku run php artisan optimize --exit-code --app $HEROKU_APP
  when: manual

cleanup_table:
  image: lorisleiva/laravel-docker:7.4
  stage: cleanup_table
  script:
    - apk add ruby ruby-master ruby-irb ruby-rake ruby-io-console ruby-bigdecimal ruby-json ruby-bundler yarn ruby-rdoc >> /master/null
    - apk update
    - gem install dpl >> /master/null
    - dpl --provider=heroku --app=$HEROKU_APP --api-key=$HEROKU_API_KEY
    - yarn global add heroku@7.42.1
    - heroku run php artisan migrate:fresh --seed --force --exit-code --app $HEROKU_APP
    - heroku run php artisan optimize --exit-code --app $HEROKU_APP
  when: manual
